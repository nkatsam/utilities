 #!/usr/bin/python3
 import sys
 import codecs
 from suds.client import Client

 input_file = sys.argv[1]
 out_file = sys.argv[2]
 non_words_file = sys.argv[3]
 conversionString = codecs.open(input_file,encoding='utf-8')

 # path to the web service wsdl
 wsdl = "http://dl.ilsp.gr/UP/WS/service.php?class=grTOph&wsdl"
 my_options = dict(actor='http://schema.jpapg.gr', trace=True )

 # initialize soap client
 client = Client(wsdl)
 client.set_options(soapheaders=my_options)

 with open(out_file, 'w', encoding='utf-8') as oov_prons:
     with open(non_words_file, 'w', encoding='utf-8') as non_words:

         for ln in conversionString:
             ln = ln.rstrip()

             # call public method
             res = client.service.Convert(ln)
             if res:
                 res = res.rstrip()
                 oov_prons.write("{} {}\n".format(ln,res))
             else:
                 non_words.write(ln+"\n")

 conversionString.close()
